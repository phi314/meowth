const gulp = require('gulp');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const terser = require('gulp-terser');
const sass = require('gulp-sass');
const concat = require('gulp-concat');

const paths = {
    styles: {
        src: 'src/sass/*.scss',
        dest: 'dist/assets/css'
    },
    scripts: {
        src: 'src/js/*.js',
        dest: 'dist/assets/js'
    },
    html: {
        src: 'src/*.html',
        dest: 'dist'
    },
    images: {
        src: 'src/images/*',
        dest: 'dist/images'
    }, 
    vendorStyles: {
        src: 'src/vendor/**/*.css',
        dest: 'dist/assets/css'
    },
    vendorScripts: {
        src: 'src/vendor/**/*.js',
        dest: 'dist/assets/js'
    },
    favicon: {
        src: 'src/images/favicon.ico',
        dest: 'dist'
    }

}

function message(){
    return console.log('Gulp task is running...');
}

// Copy html
function html(){
    return gulp.src(paths.html.src)
        .pipe(gulp.dest(paths.html.dest));
};

// Sass
function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sass(
            {outputStyle: 'compressed'}
        ).on('error', sass.logError))
        .pipe(gulp.dest(paths.styles.dest))
};

// Minify
function scripts(){
    return gulp.src(paths.scripts.src)
        .pipe(concat('main.js'))
        // .pipe(uglify()) // Not supported ES6
        .pipe(terser())
        .pipe(gulp.dest(paths.scripts.dest));
}

// Optimize images
function images() {
	return gulp.src(paths.images.src)
		.pipe(imagemin())
		.pipe(gulp.dest(paths.images.dest))
};

// Vendor Styles CSS
function vendorStyles() {
    return gulp.src(paths.vendorStyles.src)
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(paths.vendorStyles.dest))
}

// Vendor Scripts JS
function vendorScripts() {
    return gulp.src(paths.vendorScripts.src)
        .pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(paths.vendorScripts.dest))
}

// Favicon
function favicon() {
    return gulp.src(paths.favicon.src)
        .pipe(gulp.dest(paths.favicon.dest))
}

// Watch 
function watch() {
    gulp.watch(paths.html.src, html);
    gulp.watch(paths.styles.src, styles);
    gulp.watch(paths.scripts.src, scripts);
    gulp.watch(paths.images.src, images);
    gulp.watch(paths.favicon.src, favicon);
    gulp.watch(paths.vendorStyles.src, vendorStyles);
    gulp.watch(paths.vendorScripts.src, vendorScripts);
}

var vendor = gulp.series(gulp.parallel(vendorStyles, vendorScripts));
var build = gulp.series(gulp.parallel(message, html, styles, scripts, images, favicon, vendor));

exports.message = message;
exports.html = html;
exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.favicon = favicon;
exports.vendorStyles = vendorStyles;
exports.vendorScripts = vendorScripts;
exports.vendor = vendor;
exports.watch = watch;
exports.build = build;

exports.default = build;